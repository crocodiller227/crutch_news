<?php
/**
 * Created by PhpStorm.
 * User: dmitrystepanov
 * Date: 9/29/18
 * Time: 10:59 AM
 */

namespace App\DataFixtures;


use App\Entity\Category;
use App\Entity\Tag;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadTagData extends Fixture implements  ContainerAwareInterface
{
    public const TAGS_FIXTURES = 'tags_fixtures_';
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $tags_data = ['путин-краб', 'велоспорт', 'парламент', 'митинг', 'собрание', 'субботник', 'потрачено', 'как так-то?!', 'и так сойдет', 'срань господня, да быть того не может'];

        $tags = [];
        foreach ($tags_data as $title) {
            $tag = new Tag();
            $tag
                ->setTitle($title);
            $manager->persist($tag);
            array_push($tags, $tag);
        }
        $manager->flush();

        foreach ($tags as $key => $tag){
            $this->addReference(self::TAGS_FIXTURES . $key, $tag);
        }
    }
}
