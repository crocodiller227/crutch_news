<?php
/**
 * Created by PhpStorm.
 * User: dmitrystepanov
 * Date: 9/29/18
 * Time: 10:59 AM
 */

namespace App\DataFixtures;


use App\Entity\Category;
use App\Entity\News;
use App\Entity\NewsQuality;
use App\Entity\NewsRegard;
use App\Entity\NewsRelevance;
use App\Entity\Tag;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadNewsRateData extends Fixture implements ContainerAwareInterface, DependentFixtureInterface
{
    public const NEWS_FIXTURES = 'news_fixtures_';
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {


        $news = [];
        for ($i = 0; $i < 9; $i++) {
            array_push($news, $this->getReference(LoadNewsData::NEWS_FIXTURES . $i));
        }

        $users = [];
        for ($i = 0; $i < 5; $i++) {
            array_push($users, $this->getReference(LoadUserData::USERS_FIXTURES . $i));
        }
        $rates = [-1, 1];
        foreach ($news as $one_news) {
            $rate_quality = new NewsQuality();
            $rate_quality
                ->setNews($one_news)
                ->setRate($rates[array_rand($rates)])
                ->setRater($users[array_rand($users)]);

            $rate_regard = new NewsRegard();
            $rate_regard
                ->setNews($one_news)
                ->setRate($rates[array_rand($rates)])
                ->setRater($users[array_rand($users)]);

            $rate_relevance = new NewsRelevance();
            $rate_relevance
                ->setNews($one_news)
                ->setRate($rates[array_rand($rates)])
                ->setRater($users[array_rand($users)]);

            $manager->persist($rate_quality);
            $manager->persist($rate_regard);
            $manager->persist($rate_relevance);
        }
        $manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            LoadNewsData::class,
            LoadUserData::class,
        ];
    }
}
