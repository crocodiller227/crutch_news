<?php
/**
 * Created by PhpStorm.
 * User: dmitrystepanov
 * Date: 9/29/18
 * Time: 10:59 AM
 */

namespace App\DataFixtures;


use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadCategoryData extends Fixture implements ContainerAwareInterface
{
    public const CATEGORIES_FIXTURES = 'categories_fixtures_';
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $categories_data = [
            'titles' => ['Общество', 'Политика', 'Спорт'],
            'descriptions' => [
                'В данную категорию непосредственно следует помещать информацию об организации групп людей и учреждений, о религиозных, культурных, научных, политических, экономических и прочих феноменах.',
                'Новости в категории «Политика»',
                'Для страниц по теме Спорт',
            ],
        ];
        $categories = [];
        foreach ($categories_data['titles'] as $key => $title) {
            $category = new Category();
            $category
                ->setTitle($title)
                ->setDescription($categories_data['descriptions'][$key]);
            $manager->persist($category);
            array_push($categories, $category);
        }
        $manager->flush();

        foreach ($categories as $key => $category) {
            $this->addReference(self::CATEGORIES_FIXTURES . $key, $category);
        }
    }
}
