<?php
/**
 * Created by PhpStorm.
 * User: dmitrystepanov
 * Date: 9/29/18
 * Time: 10:59 AM
 */

namespace App\DataFixtures;


use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUserData extends Fixture implements ContainerAwareInterface{

    public const USERS_FIXTURES = 'users_fixtures_';

    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $users_data = [
            'usernames' => ['admin', 'john_doe', 'vasya_pupkin', 'petya_petrov', 'ivan_ivanov'],
            'fullnames' => ['ADMIN', 'John Doe', 'Vasiliy pupkin', 'Petr Petrov', 'Ivan Ivanov'],
        ];

        $users = [];

        foreach ($users_data['usernames'] as $key => $username) {
            $user = new User();
            $user
                ->setUsername($username)
                ->setEnabled(true)
                ->setFullName($users_data['fullnames'][$key])
                ->setPlainPassword('root')
                ->setEmail($username . '@anymailservice.domainzone');
            if ($users_data['usernames'][$key] === 'admin') {
                $user->addRole('ROLE_ADMIN');
            }
            $manager->persist($user);
            array_push($users, $user);
        }
        $manager->flush();

        foreach ($users as $key => $user){
            $this->addReference(self::USERS_FIXTURES . $key, $user);
        }
    }
    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    function getOrder()
    {
        return 1;
    }
}
