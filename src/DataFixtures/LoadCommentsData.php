<?php
/**
 * Created by PhpStorm.
 * User: dmitrystepanov
 * Date: 9/29/18
 * Time: 10:59 AM
 */

namespace App\DataFixtures;


use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\News;
use App\Entity\NewsQuality;
use App\Entity\NewsRegard;
use App\Entity\NewsRelevance;
use App\Entity\Tag;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadCommentsData extends Fixture implements ContainerAwareInterface, DependentFixtureInterface
{
    public const NEWS_FIXTURES = 'news_fixtures_';
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {


        $news = [];
        for ($i = 0; $i < 9; $i++) {
            array_push($news, $this->getReference(LoadNewsData::NEWS_FIXTURES . $i));
        }

        $users = [];
        for ($i = 0; $i < 5; $i++) {
            array_push($users, $this->getReference(LoadUserData::USERS_FIXTURES . $i));
        }
        $comments = [
            'Да у нас всегда так',
            'Копать-колотить',
            'Это специально, чтобы собрать лайков',
            'Не зашло =(',
            'Афтар, убейся ап стену',
            'Фу, кремлебот, ватники!!!!!!!!!!!"',
            'Я твой дом труба шатал',
            'Это невероятно!'
        ];
        foreach ($news as $one_news) {
           foreach ($users as $user){
               $comment = new Comment();
               $comment
                   ->setNews($one_news)
                   ->setAuthor($user)
                   ->setComment($comments[array_rand($comments)]);
               $manager->persist($comment);
           }
        }
        $manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            LoadNewsData::class,
            LoadUserData::class,
        ];
    }
}
