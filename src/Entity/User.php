<?php
/**
 * Created by PhpStorm.
 * User: dmitrystepanov
 * Date: 9/29/18
 * Time: 9:32 AM
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\OneToMany;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var ArrayCollection
     * @OneToMany(targetEntity="News", mappedBy="author")
     */
    private $news;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $fullName;

    public function __construct()
    {
        parent::__construct();
        $this->addRole('ROLE_USER');
        $this->news = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $fullName
     * @return User
     */
    public function setFullName(string $fullName) :self
    {
        $this->fullName = $fullName;
        return $this;
    }

    /**
     * @return string
     */
    public function getFullName():?string
    {
        return $this->fullName;
    }

    /**
     * @param ArrayCollection $news
     * @return User
     */
    public function setNews(ArrayCollection $news): User
    {
        $this->news = $news;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getNews(): ArrayCollection
    {
        return $this->news;
    }

    /**
     * @return bool
     */
    public function isAdmin(): bool
    {
        return in_array('ROLE_ADMIN', $this->roles);
    }

}