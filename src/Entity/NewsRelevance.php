<?php
/**
 * Created by PhpStorm.
 * User: dmitrystepanov
 * Date: 9/29/18
 * Time: 1:29 PM
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Entity
 * @ORM\Table(name="news_relevance")
 */
class NewsRelevance extends AbstractNewsRate
{
    /**
     * @var News
     * @ManyToOne(targetEntity="News", inversedBy="relevance")
     * @JoinColumn(name="news", referencedColumnName="id")
     */
    protected $news;
}