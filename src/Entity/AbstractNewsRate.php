<?php
/**
 * Created by PhpStorm.
 * User: dmitrystepanov
 * Date: 9/29/18
 * Time: 2:05 PM
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

class AbstractNewsRate
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    protected $news;

    /**
     * @var User
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="rater", referencedColumnName="id")
     */
    protected $rater;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    protected $rate;

    /**
     * @param int $id
     * @return AbstractNewsRate
     */
    public function setId(int $id): AbstractNewsRate
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param News $news
     * @return AbstractNewsRate
     */
    public function setNews(News $news): AbstractNewsRate
    {
        $this->news = $news;
        return $this;
    }

    /**
     * @return News
     */
    public function getNews(): News
    {
        return $this->news;
    }

    /**
     * @param User $rater
     * @return AbstractNewsRate
     */
    public function setRater(User $rater): AbstractNewsRate
    {
        $this->rater = $rater;
        return $this;
    }

    /**
     * @return User
     */
    public function getRater(): User
    {
        return $this->rater;
    }

    /**
     * @return int
     */
    public function getRate(): int
    {
        return $this->rate;
    }

    /**
     * @param int $rate
     * @return AbstractNewsRate
     */
    public function setRate(int $rate): AbstractNewsRate
    {
        $this->rate = $rate;
        return $this;
    }

    /**
     * @return AbstractNewsRate
     */
    public function positiveRate(): self
    {
        $this->rate = 1;
        return $this;
    }

    /**
     * @return AbstractNewsRate
     */
    public function negativeRate() : self
    {
        $this->rate = -1;
        return $this;
    }
}