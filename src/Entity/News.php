<?php
/**
 * Created by PhpStorm.
 * User: dmitrystepanov
 * Date: 9/29/18
 * Time: 11:39 AM
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\PersistentCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="news")
 */
class News
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $body;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="news")
     * @ORM\JoinColumn(name="author", referencedColumnName="id")
     */
    private $author;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $publicationDate;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $createDate;

    /**
     * @var Category
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="news")
     * @ORM\JoinColumn(name="category", referencedColumnName="id")
     */
    private $category;

    /**
     * @var PersistentCollection
     * @ManyToMany(targetEntity="Tag", inversedBy="news")
     * @JoinTable(name="news_tags")
     */
    private $tags;

    /**
     * @var PersistentCollection
     * @ORM\OneToMany(targetEntity="NewsQuality", mappedBy="news")
     */
    private $quality;

    /**
     * @var PersistentCollection
     * @ORM\OneToMany(targetEntity="NewsRelevance", mappedBy="news")
     */
    private $relevance;

    /**
     * @var PersistentCollection
     * @ORM\OneToMany(targetEntity="NewsRegard", mappedBy="news")
     */
    private $regard;

    /**
     * @var PersistentCollection
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="news")
     */
    private $comments;

    /**
     * News constructor.
     */
    public function __construct()
    {
        $this->createDate = new \DateTime();
        $this->tags = new ArrayCollection();
        $this->quality = new ArrayCollection();
        $this->relevance = new ArrayCollection();
        $this->regard = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    /**
     * @param int $id
     * @return News
     */
    public function setId(int $id): News
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param string $title
     * @return News
     */
    public function setTitle(string $title): News
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $body
     * @return News
     */
    public function setBody(string $body): News
    {
        $this->body = $body;
        return $this;
    }

    /**
     * @return string
     */
    public function getBody(): ?string
    {
        return $this->body;
    }

    /**
     * @param User $author
     * @return News
     */
    public function setAuthor(User $author): News
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return User
     */
    public function getAuthor(): ?User
    {
        return $this->author;
    }

    /**
     * @param \DateTime $publicationDate
     * @return News
     */
    public function setPublicationDate(\DateTime $publicationDate): News
    {
        $this->publicationDate = $publicationDate;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPublicationDate(): ?\DateTime
    {
        return $this->publicationDate;
    }

    /**
     * @param \DateTime $createDate
     * @return News
     */
    public function setCreateDate(\DateTime $createDate): News
    {
        $this->createDate = $createDate;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreateDate(): ?\DateTime
    {
        return $this->createDate;
    }

    /**
     * @param Category $category
     * @return News
     */
    public function setCategory(Category $category): News
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return Category
     */
    public function getCategory(): ?Category
    {
        return $this->category;
    }

    /**
     * @return News
     */
    public function setTags($tags): News
    {
        $this->tags = $tags;
        return $this;
    }


    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param Tag $tag
     * @return News
     */
    public function addTag(Tag $tag): self
    {
        $this->tags->add($tag);
        return$this;
    }

    /**
     * @return News
     */
    public function setQuality($quality): News
    {
        $this->quality = $quality;
        return $this;
    }

    /**
     * @return PersistentCollection
     */
    public function getQuality()
    {
        return $this->quality;
    }

    /**
     * @return News
     */
    public function setRelevance($relevance): News
    {
        $this->relevance = $relevance;
        return $this;
    }


    public function getRelevance()
    {
        return $this->relevance;
    }

    /**
     * @return News
     */
    public function setRegard($regard): News
    {
        $this->regard = $regard;
        return $this;
    }

    /**
     * @return PersistentCollection
     */
    public function getRegard()
    {
        return $this->regard;
    }

    /**
     * @return bool
     */
    public function isPublished(): bool
    {
        if(!$this->publicationDate){
            return false;
        }

        $currentDate = new \DateTime();
        return $currentDate->getTimestamp() >= $this->publicationDate->getTimestamp();
    }

    
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @return News
     */
    public function setComments($comments): News
    {
        $this->comments = $comments;
        return $this;
    }

}