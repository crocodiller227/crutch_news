<?php

namespace App\Repository;

use App\Entity\News;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method News|null find($id, $lockMode = null, $lockVersion = null)
 * @method News|null findOneBy(array $criteria, array $orderBy = null)
 * @method News[]    findAll()
 * @method News[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NewsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, News::class);
    }

    /**
     * @param $tag_id
     * @return mixed
     */
    public function getNewsByTag($tag_id)
    {
        return $this->createQueryBuilder('n')
            ->innerJoin('n.tags', 't', 'WITH', 't.id = :tag_id')
            ->setParameter('tag_id', $tag_id)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $category
     * @param array $tags
     * @return mixed
     */
    public function getByCategoryAndTags($category, array $tags)
    {
        return $this->createQueryBuilder('n')
            ->innerJoin('n.category', 'c', 'WITH', 'c.id = :category_id')
            ->setParameter('category_id', $category)
            ->innerJoin('n.tags', 't', 'WITH', 't.id in (:tags)')
            ->setParameter('tags', $tags)
            ->getQuery()
            ->getResult();
    }


    public function getArrayRates($id)
    {
        try {
            return $this->createQueryBuilder('n')
                ->select('SUM(q.rate), SUM(r.rate), COUNT(n)')
                ->innerJoin('n.quality', 'q')
                ->innerJoin('n.relevance', 'r')
                ->where('n.author = :user')
                ->setParameter('user', $id)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
        }
    }
}
