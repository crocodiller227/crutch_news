<?php

namespace App\Repository;

use App\Entity\NewsRegard;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method NewsRegard|null find($id, $lockMode = null, $lockVersion = null)
 * @method NewsRegard|null findOneBy(array $criteria, array $orderBy = null)
 * @method NewsRegard[]    findAll()
 * @method NewsRegard[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NewsRegardRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, NewsRegard::class);
    }

    public function findByNewsAndUser(int $news_id, $user_id)
    {
        return $this->createQueryBuilder('r')
            ->select('r')
            ->where('r.news = :news_id')
            ->andWhere('r.rater = :user_id')
            ->setParameter('news_id', $news_id)
            ->setParameter('user_id', $user_id)
            ->getQuery()
            ->getResult();
    }
}
