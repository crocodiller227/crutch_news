<?php
/**
 * Created by PhpStorm.
 * User: dmitrystepanov
 * Date: 9/29/18
 * Time: 6:09 PM
 */

namespace App\FormType;


use App\Entity\Category;
use App\Entity\News;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewsFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('category', EntityType::class, [
                'class'        => 'App:Category',
                'choice_label' => 'title',
                'label' => 'Категория',
                'choice_value' => 'id',
                'expanded'  => true,
                'multiple'  => false,
            ])
            ->add('tags', EntityType::class, [
                'class'        => 'App:Tag',
                'choice_label' => 'title',
                'choice_value' =>'id',
                'label' => 'Теги',
                'expanded'  => false,
                'multiple'  => true,
            ])
            ->add('Find', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => News::class,
        ]);
    }
}