<?php
/**
 * Created by PhpStorm.
 * User: dmitrystepanov
 * Date: 9/29/18
 * Time: 4:23 PM
 */

namespace App\Controller;


use App\Entity\Comment;
use App\Entity\News;
use App\Entity\NewsQuality;
use App\Entity\NewsRegard;
use App\Entity\NewsRelevance;
use App\FormType\CommentType;
use App\FormType\NewsAddType;
use App\FormType\NewsFilterType;
use App\Repository\CommentRepository;
use App\Repository\NewsQualityRepository;
use App\Repository\NewsRegardRepository;
use App\Repository\NewsRelevanceRepository;
use App\Repository\NewsRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface as EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NewsController extends Controller
{
    /**
     * @Route("/", name="index_action")
     * @param NewsRepository $newsRepository
     * @param Request $request
     * @return Response
     */
    public function indexAction(
        NewsRepository $newsRepository,
        Request $request
    )
    {
        $news = $newsRepository->findAll();

        $filter_form = $this->createForm(NewsFilterType::class);
        $filter_form->handleRequest($request);
        if ($filter_form->isSubmitted()){
            $news = $filter_form->getData();
            $tags = [];
            $category = $news->getCategory()->getId();
            foreach ($news->getTags() as $tag){
                array_push($tags, $tag->getId());
            }
            $news = $newsRepository->getByCategoryAndTags($category, $tags);

        }

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $news,
            $request->query->getInt('page', 1),
            5
        );

        return $this->render('index.html.twig', [
            'pagination' => $pagination,
            'filter' => $filter_form->createView()
            ]);
    }

    /**
     * @Route("/news/show/{id}", name="news_show")
     * @param int $id
     * @param NewsRepository $newsRepository
     * @param Request $request
     * @param EntityManager $em
     * @return Response
     */
    public function newsShowAction(
        int $id,
        NewsRepository $newsRepository,
        Request $request,
        EntityManager $em

    )
    {
        $news = $newsRepository->findOneBy(['id' => $id]);
        $form = $this->createForm(CommentType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted()){
            $comment = new Comment();

            $comment
                ->setNews($news)
                ->setAuthor($this->getUser())
                ->setComment($form->getData()['comment']);
            $em->persist($comment);
            $em->flush();

        }

        return $this->render('show_news.html.twig', [
            'news' => $news,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/new/show/tag/{id}", name="show_news_by_tag")
     * @param int $id
     * @param NewsRepository $newsRepository
     * @param Request $request
     * @return Response
     */
    public function showNewsByTag(
        int $id,
        NewsRepository $newsRepository,
        Request $request
    )
    {
        $news = $newsRepository->getNewsByTag($id);

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $news,
            $request->query->getInt('page', 1),
            5
        );

        $filter_form = $this->createForm(NewsFilterType::class);

        return $this->render('index.html.twig', [
            'pagination' => $pagination,
            'filter' => $filter_form->createView()
        ]);
    }

    /**
     * @Route("news/rate/{id}/{param}/{rate}", name="news_rate")
     * @param int $id
     * @param string $param
     * @param string $rate
     * @param NewsRepository $newsRepository
     * @param EntityManager $em
     * @param NewsQualityRepository $newsQualityRepository
     * @param NewsRegardRepository $newsRegardRepository
     * @param NewsRelevanceRepository $relevanceRepository
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function newsRateAction(
        int $id,
        string $param,
        string $rate,
        NewsRepository $newsRepository,
        EntityManager $em,
        NewsQualityRepository $newsQualityRepository,
        NewsRegardRepository $newsRegardRepository,
        NewsRelevanceRepository $relevanceRepository
    )
    {
        switch ($param){
            case 'quality':
                if (!$newsQualityRepository->findByNewsAndUser($id, $this->getUser()->getId())) {
                    $rate_entt = new NewsQuality();
                } else{
                    $this->addFlash('error', 'Вы уже голосавали');
                }
                break;
            case 'relevance':
                if (!$relevanceRepository->findByNewsAndUser($id, $this->getUser()->getId())) {
                    $rate_entt = new NewsRelevance();
                } else{
                    $this->addFlash('error', 'Вы уже голосавали');
                }
                break;
            case 'regard':
                if (!$newsRegardRepository->findByNewsAndUser($id, $this->getUser()->getId())) {
                    $rate_entt = new NewsRegard();
                } else{
                    $this->addFlash('error', 'Вы уже голосавали');
                }
                break;
            default:
                $this->addFlash('error', 'Что-то пошло не так');
                break;

        }

        if (isset($rate_entt)){
            $rate_entt
                ->setNews($newsRepository->findOneBy(['id' => $id]))
                ->setRater($this->getUser())
                ->setRate(($rate == '+')? 1 : -1);

            $em->persist($rate_entt);
            $em->flush();

        }
        return $this->redirectToRoute('news_show', ['id' => $id]);
    }

    /**
     * @Route("/news/{news_id}/comment/delete/{id}", name="delete_comment")
     * @param int $id
     * @param int $news_id
     * @param EntityManager $em
     * @param CommentRepository $commentRepository
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteCommentAction(
        int $id,
        int $news_id,
        EntityManager $em,
        CommentRepository $commentRepository
    )
    {
        if ($this->getUser()->isAdmin())
        {
            $em->remove($commentRepository->findOneBy(['id' => $id]));
            $em->flush();
        }
        return $this->redirectToRoute('news_show', ['id' => $news_id]);
    }

    /**
     * @Route("/news/add", name="news_add")
     * @param Request $request
     * @param EntityManager $em
     * @return Response
     */
    public function addNewsAction(
        Request $request,
        EntityManager $em
    )
    {
        $news = new News();
        $form = $this->createForm(NewsAddType::class, $news);
        $form->handleRequest($request);


        if ($form->isSubmitted()){
            $news->setAuthor($this->getUser());
            $em->persist($news);
            $em->flush();

            $form_filter = $this->createForm(NewsFilterType::class);
            return $this->redirectToRoute('index_action', ['filter' => $form_filter->createView()]);
        }

        return $this->render('create_news.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/news/author/{id}", name="show_author")
     * @param int $id
     * @param UserRepository $repository ,
     * @param NewsRepository $newsRepository
     * @return Response
     */
    public function showAuthorProfileAction(
        int $id,
        UserRepository $repository,
        NewsRepository $newsRepository
    )
    {
        $user = $repository->findOneBy(['id' => $id]);
        $rating = array_sum($newsRepository->getArrayRates($id))/3;

        return $this->render('author_page.html.twig', [
            'user' => $user,
            'rating' => $rating
        ]);
    }

    /**
     * @Route("/news/publish_date/{id}", name="add_publication_date")
     * @param Request $request
     * @param int $id
     * @param NewsRepository $newsRepository
     * @param EntityManager $entityManager
     * @return Response
     */
    public function addPublishDateAction(
        Request $request,
        int $id,
        NewsRepository $newsRepository,
        EntityManager $entityManager
    )
    {
        if ($this->getUser()->isAdmin()){
            $news = $newsRepository->findOneBy(['id' => $id]);
            $news->setPublicationDate(new \DateTime($request->request->get('datepicker')));
            $entityManager->flush();
        }

        return $this->redirectToRoute('news_show', ['id' => $id]);
    }
}